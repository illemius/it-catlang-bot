"""
Automatic generated file!
Illemius/Nucleus/ProjectBuilder (0.2.1)
"""

import logging
import os
import sys

from pytz import timezone

import telebot

BUILD = 'b0038'
# Debugging
DEBUG = True

# TZ is unused..
TIMEZONE = timezone('Europe/Kiev')

# Base application dir
# Usage: os.path.join(<BASE_DIR or APP_DIR>, '<file.name>')
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APP_DIR = os.path.dirname(os.path.realpath(sys.argv[0]))

# === Telegram ===
BOT_URL = 'https://telegram.me/IT_Catlang_bot'
TOKEN = '<TOKEN>'
WEBHOOK_URL = 'catlangstat.illemius.xyz'
WEBHOOK_PORT = 37528  # Use nginx proxy
WEBHOOK_LISTEN = '0.0.0.0'

WEBHOOK_SSL_CERT = '/etc/letsencrypt/live/{}/cert.pem'.format(WEBHOOK_URL)  # Path to the ssl certificate
WEBHOOK_SSL_PRIV = '/etc/letsencrypt/live/{}/privkey.pem'.format(WEBHOOK_URL)  # Path to the ssl private key

WEBHOOK_URL_BASE = "https://%s" % WEBHOOK_URL
WEBHOOK_URL_PATH = "/%s" % 'webhook'

bot = telebot.TeleBot(TOKEN)

# === Logging ===
LOG_LEVEL = logging.DEBUG
CRASHREPORT_LOCATION = os.path.join(APP_DIR, 'crash')
REPORTS_LOCATION = os.path.join(APP_DIR, 'dumps')

# === MongoDB & MongoEngine ===
MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_DATABASE = 'itcatstat'

# === Languages ===
LANG_LIST = [
    'Action Script',
    'Ada',
    'Algol',
    'Asm',
    'Basic',
    'BrainFuck',
    'C#',
    'C',
    'C++',
    'Cobol',
    'Delphi',
    'Erlang',
    'Fortran',
    'Go',
    'Java',
    'JavaScript',
    'Lisp',
    'Lua',
    'MATLAB',
    'Object Pascal',
    'Objective-C',
    'Pascal',
    'pawno',
    'Perl',
    'PHP',
    'Prolog',
    'Python',
    'QBASIC',
    'Ruby',
    'sh',
    'Simula',
    'Smalltalk',
    'Swift',
    'Visual Basic',
    'Icon',
    'OCaml',
    'Haskell',
    'Rust',
    'Petooh'
]
