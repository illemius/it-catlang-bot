def hash_code(text: str) -> int:
    """
    Hash text
    :param text:
    :return:
    """
    result = 0
    for symbol in text:
        result = ord(symbol) + ((result << 5) - result)
    return result


def int_to_hex_color(i: int) -> str:
    """
    Int color to hex
    :param i:
    :return:
    """
    result = "#%0.2X" % i
    return result[:7]


def color_from_str(text):
    """
    Generate color based on text hash
    :param text:
    :return:
    """
    return int_to_hex_color(hash_code(text))
