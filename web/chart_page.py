import json

from models.catlang import Lang
from web.utils import color_from_str

# TODO: Remake page. Maybe use Jinja2 (But it's needed for one page? I don't think so)

TEMPLATE = """
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>IT-CatLang Statistic Bot</title>

    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages': ['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart() {

            // Create the data table.
            var data = google.visualization.arrayToDataTable(
                    %s
            )

            // Set chart options
            var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight|| g.clientHeight;

            var options = {
                'title': 'Статистика языков программирования, собранная ботом',
                'width': x - 0.12 * x,
                'height': y - 0.12 * y,
                'bar': {'groupWidth': "100%%"},
                'legend': {'position': "none" },
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>
</head>

<body>
<div id="chart_div"></div>
</body>
</html>
"""


def charts_page():
    data = [['Язык программирования', 'Количество людей', {'role': 'style'}, {'role': 'annotation'}]]
    users = []
    stat = {}

    langs = Lang.objects

    for lang in langs:
        stat[lang.lang] = stat.get(lang.lang, 0) + 1
        if lang.user_id not in users:
            users.append(lang.user_id)

    users_count = len(users)
    for key, value in sorted(stat.items(), key=lambda kv: kv[1], reverse=True):
        percent = (value / users_count) * 100
        data.append([key, value, color_from_str(key), '%d%%' % percent])

    return TEMPLATE % json.dumps(data, ensure_ascii=True)
