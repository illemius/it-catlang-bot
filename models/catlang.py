import datetime
from mongoengine import *


class Dialog(Document):
    dialog_id = IntField()
    title = StringField()
    creation_date = DateTimeField(default=datetime.datetime.now)
    modified_date = DateTimeField(default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        if not self.creation_date:
            self.creation_date = datetime.datetime.now()
        self.modified_date = datetime.datetime.now()
        return super(Dialog, self).save(*args, **kwargs)

    @classmethod
    def is_inited(cls, dialog_id):
        dialogs = Dialog.objects(dialog_id=dialog_id)
        return len(dialogs) > 0


class Lang(Document):
    user_id = IntField()
    lang = StringField()


class LangEntity(Document):
    title = StringField()
