"""
Python telegram bot

Illemius/BotCore/IT-CatLangBot

Special for @it_cat_encrypted
Customer: @mefest666
"""

import logging

import flask
from flask import Flask, redirect
from mongoengine import connect

import telebot
from config import bot, MONGO_DATABASE, MONGO_HOST, MONGO_PORT, WEBHOOK_PORT, WEBHOOK_URL_PATH, WEBHOOK_URL, BOT_URL, \
    WEBHOOK_LISTEN
from core.logging import dmesg, get_logger
from core.utils import get_username_or_name
from web.chart_page import charts_page

# TODO: Move languages list to DB
# TODO: Add languages list editor
# TODO: Change web server to http.SimpleHTTPServer or finish Illemius/Nucleus/WebServer
# TODO: Use Illemius/Nucleus/Locale util.

connect(
    db=MONGO_DATABASE,
    host=MONGO_HOST,
    port=MONGO_PORT
)

dmesg('Init app', group='bot')
telebot.logger = get_logger('telegram')
telebot.logger.setLevel(logging.DEBUG)

if __name__ == '__main__':
    app = Flask(__name__)

    # Import all chat handlers
    from commands import *

    @app.route('/', methods=['GET', 'HEAD'])
    def index():
        """
        Index page
        Always redirect to bot page
        :return:
        """
        return redirect(BOT_URL, code=302)

    @app.route('/charts', methods=['GET', 'HEAD'])
    def chart():
        """
        Generate statistic charts
        :return:
        """
        return charts_page()

    # Process webhook calls
    @app.route(WEBHOOK_URL_PATH, methods=['POST'])
    def webhook():
        """
        Base of webhook
        :return:
        """
        # TODO: NEED MORE LOGS
        if flask.request.headers.get('content-type') == 'application/json':
            request = flask.request.get_data().decode("utf-8")
            update = telebot.types.Update.de_json(request)
            if update.message:
                telebot.logger.info('Message from {user} ({user_id}) in chat {chat}: "{text}"'.format(
                    user=get_username_or_name(update.message.from_user),
                    user_id=update.message.from_user.id,
                    chat=update.message.chat.id,
                    text=update.message.text
                ))
                bot.process_new_messages([update.message])
            if update.edited_message:
                bot.process_new_edited_messages([update.edited_message])
            if update.inline_query:
                bot.process_new_inline_query([update.inline_query])
            if update.chosen_inline_result:
                bot.process_new_chosen_inline_query([update.chosen_inline_result])
            if update.callback_query:
                bot.process_new_callback_query([update.callback_query])
            return 'ok'
        else:
            flask.abort(403)

    # Setup webhook
    bot.remove_webhook()
    bot.set_webhook(url=WEBHOOK_URL + WEBHOOK_URL_PATH)

    # Setup web server
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    # app.debug = True
    app.run(host=WEBHOOK_LISTEN, port=WEBHOOK_PORT)
