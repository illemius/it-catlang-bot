import sys

import telebot
from config import bot
from core.logging import CrashReport


def always_get(data, key, default=None, getter=None):
    """
    Always get value

    (Illemius/Nucleus/utils)
    :param data: target
    :param key: key
    :param default: default value
    :param getter: lambda function for specific search
    :return: result or default
    """
    try:
        if callable(getter):
            return getter(key, default)
        if type(data) is dict:
            return data.get(key, default)
        elif type(data) in [list, str] and type(key) == int:
            if key < len(data):
                return data[key]
        elif hasattr(data, '__dict__'):
            return data.__dict__.get(key, default)
    except:
        pass
    return default


def typing(message):
    bot.send_chat_action(message.chat.id, 'typing')


def get_username_or_name(user):
    if user.username is None:
        name = '{} {}'.format(user.first_name or '', user.last_name or '').strip()
        return name if len(name) else user.id
    return '@' + user.username


def crash_message(message):
    c = CrashReport(*sys.exc_info())
    c.save()
    telebot.logger.error('\n\t'.join(c.formatted_traceback))
    text = 'Во время выполнения команды произошла ошибка :с\n' \
           'Если не сложно, перешли, пожалуйста это сообщение @JrootJunior\n' \
           'Report: {id}-{file}'.format(id=c.id, file=c.filename)
    if type(message) is int:
        return bot.send_message(message, text)
    bot.reply_to(message, text)


def get_num_ending(number: int, endings: tuple or list) -> str:
    """
    Feature returns the ending for plural words based on the number and array of endings

    Source (JS/PHP): https://habrahabr.ru/post/105428/
    :param number: The number by which you want to generate the end
    :param endings: Array (or tuple) of words or endings for numbers (1, 4, 5) e. g. ('яблоко', 'яблока', 'яблок')
    :return:
    """
    num = number % 100
    if 11 <= num <= 19:
        return endings[2]
    num %= 10
    if num == 1:
        return endings[0]
    elif 2 <= num <= 4:
        return endings[1]
    return endings[2]
