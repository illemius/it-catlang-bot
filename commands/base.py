import telebot
from config import bot, LANG_LIST, WEBHOOK_URL_BASE
from core.utils import get_username_or_name, get_num_ending, crash_message, typing
from models.catlang import Lang


def is_new_chat_member(message):
    # Bicycle
    return bool(message.new_chat_member)
    # return message.new_chat_members is not None


def ask_language(message, handle_exception=True):
    markup = telebot.types.InlineKeyboardMarkup(3)
    buttons = []
    for lang in sorted(LANG_LIST, key=lambda s: s.lower()):
        buttons.append(telebot.types.InlineKeyboardButton(lang, callback_data=lang))
    markup.add(*buttons)
    try:
        bot.send_message(message.from_user.id, 'На каком языке программирования пишешь?', reply_markup=markup)
        if message.chat.type != 'private':
            bot.reply_to(message, 'Я отправил тебе вопрос личным сообщением.')
    except:
        if handle_exception:
            return bot.reply_to('Произошла ошибка! Я не могу тебе написать в личку.\n'
                                'Напиши мне в ЛС и я смогу тебе отвечать.')
        raise


@bot.message_handler(commands=['start'])
def cmd_start(message):
    """
    Init dialog
    :param message:
    :return:
    """

    try:
        typing(message)
        # Command available only in groups and supergroups
        if message.chat.type == 'private':
            # bot.reply_to(message, 'Привет.')
            return ask_language(message, handle_exception=False)
        """
        # Check dialog in DB
        dialogs = Dialog.objects(dialog_id=message.chat.id)
        if len(dialogs) > 0:
            return bot.reply_to(message, 'Я уже был активирован в этом диалоге.')

        # Init dialog
        dialog = Dialog(dialog_id=message.chat.id, title=message.chat.title)
        dialog.save()

        return bot.reply_to(message, 'Теперь я буду реагировать на добавление пользователей в этом диалоге.')
        """
    except:
        crash_message(message)


@bot.message_handler(commands=['stop'])
def cmd_stop(message):
    """
    Disable dialog
    :param message:
    :return:
    """

    try:
        typing(message)
        # Command available only in groups and supergroups
        if message.chat.type not in ['group', 'supergroup']:
            return bot.reply_to(message, 'Пока.')
        """
        # Check dialog in DB
        dialogs = Dialog.objects(dialog_id=message.chat.id)
        if len(dialogs) == 0:
            return bot.reply_to(message, 'Бот не активирован в этом диалоге.')

        # Disable bot in dialog
        for dialog in dialogs:
            dialog.delete()

        return bot.reply_to(message, 'Я больше не буду реагировать на добавление пользователей в этом диалоге.')
        """
    except:
        # Handle any exception
        crash_message(message)


@bot.message_handler(commands=['lang'])
def cmd_lang(message):
    try:
        # Select target user
        # Don't work. But why?..
        typing(message)
        if bool(message.reply_to_message):
            user = message.reply_to_message.from_user
        else:
            user = message.from_user

        lang_list = Lang.objects(user_id=user.id)
        if len(lang_list):
            text = 'Пользователь {user} пишет на: {list}'.format(
                user=get_username_or_name(user),
                list=', '.join(sorted([lang.lang for lang in lang_list], key=lambda s: s.lower()))
            )
        else:
            text = 'Пользователь {user} не указал, на каких ЯП пишет.'.format(
                user=get_username_or_name(user)
            )
        bot.reply_to(message, text)
    except:
        crash_message(message)


@bot.message_handler(func=lambda message: message.chat.type == 'private' and bool(message.forward_from))
def cmd_lang_forward(message):
    try:
        typing(message)
        user = message.forward_from

        lang_list = Lang.objects(user_id=user.id)
        if len(lang_list):
            text = 'Пользователь {user} пишет на: {list}'.format(
                user=get_username_or_name(user),
                list=', '.join(sorted([lang.lang for lang in lang_list], key=lambda s: s.lower()))
            )
        else:
            text = 'Пользователь {user} не указал, на каком ЯП пишет.'.format(
                user=get_username_or_name(user)
            )
        bot.reply_to(message, text)
    except:
        crash_message(message)


"""
@bot.message_handler(content_types=['new_chat_member'])
def on_join(message):
    # o.O It must be used otherwise
    # TODO: Remake this
    if Dialog.is_inited(message.chat.id):
        objects = Lang.objects(user_id=message.new_chat_member.id)
        if not len(objects):
            bot.reply_to(message, 'Привет, ответь пожалуйста на один простой вопрос у меня в личке =)\n'
                                  'Я собираю статистику, кто какие языки программирования знает.')
            # ask_language(message)
"""


@bot.message_handler(commands=['catlang'])
def cmd_ask_language(message):
    ask_language(message, handle_exception=False)


@bot.message_handler(commands=['clear_me'])
def cmd_clear_me(message):
    user_languages = Lang.objects(user_id=message.from_user.id)
    if len(user_languages):
        for lang in user_languages:
            lang.delete()
        bot.reply_to(message, 'Список Ваших ЯП очищен.')
        telebot.logger.info(
            'User {} ({}) reset list.'.format(get_username_or_name(message.from_user), message.from_user.id))


@bot.message_handler(commands=['stat'])
def cmd_stat(message):
    try:
        users = []
        stat = {}

        langs = Lang.objects

        for lang in langs:
            stat[lang.lang] = stat.get(lang.lang, 0) + 1
            if lang.user_id not in users:
                users.append(lang.user_id)

        max_key, max_value = sorted(stat.items(), key=lambda kv: kv[1], reverse=True)[0]
        # TODO: Remove list. Not useful
        text = [
            'Статистика:',
            'Всего {} {} на вопрос.'.format(len(users), get_num_ending(len(users), (
                'пользователь ответил', 'пользователя ответили', 'пользователей ответили'))),
            'Среди них самым популярным языком программирования является {} ({} {})'.format(
                max_key, max_value, get_num_ending(max_value, ('голос', 'голоса', 'голосов'))
            ),
            'Статистика: {}'.format(WEBHOOK_URL_BASE + '/charts')
        ]

        markup = telebot.types.InlineKeyboardMarkup(1)
        markup.add(telebot.types.InlineKeyboardButton('Статистика', url=WEBHOOK_URL_BASE + '/charts'))

        return bot.reply_to(message, '\n'.join(text), reply_markup=markup, disable_web_page_preview=True)
    except:
        crash_message(message)


@bot.callback_query_handler(func=lambda query: True)
def inline_keyboard_handler(callbackquery):
    """
    Handle language selector keyboard
    :param callbackquery:
    :return:
    """
    try:
        user_languages = Lang.objects(user_id=callbackquery.from_user.id)
        if callbackquery.data not in [lang.lang for lang in user_languages]:
            record = Lang(user_id=callbackquery.from_user.id, lang=callbackquery.data)
            record.save()
            bot.answer_callback_query(callbackquery.id, 'Выбрано: {choose}'.format(choose=callbackquery.data))
            telebot.logger.info('User {} ({}) choice "{}"'.format(get_username_or_name(callbackquery.from_user),
                                                                  callbackquery.from_user.id,
                                                                  callbackquery.data))
        else:
            user_languages = user_languages.filter(lang=callbackquery.data)
            for lang in user_languages:
                lang.delete()
            bot.answer_callback_query(callbackquery.id, 'Снят выбор с: {choose}'.format(choose=callbackquery.data))
            telebot.logger.info(
                'User {} ({}) remove choice from "{}"'.format(get_username_or_name(callbackquery.from_user),
                                                              callbackquery.from_user.id,
                                                              callbackquery.data))
    except:
        crash_message(callbackquery.message.chat.id)


@bot.message_handler(commands=['all_stat', 'stat_all'])
def cmd_all_stat(message):
    try:
        typing(message)
        if message.chat.type != 'private':
            return
        users = {}
        stat_message = bot.send_message(message.chat.id, 'Сбор информации о пользователях... '
                                                         'Это займет некоторое время..')
        langs = Lang.objects

        for lang in langs:
            user_langs = users.get(lang.user_id, [])
            user_langs.append(lang.lang)
            users[lang.user_id] = user_langs

        # Slow function. Recall typing()
        typing(message)
        text = []
        for index, (user_id, data) in enumerate(users.items(), 1):
            data = ', '.join(data)
            bot.edit_message_text(
                'Сбор информации о пользователях... Это займет некоторое время..\n'
                'Process: {} users done. (total: {})\n{}'.format(
                    index, len(users), '\n'.join(text)), stat_message.chat.id, stat_message.message_id)
            try:
                user = bot.get_chat(user_id)
            except:
                text.append('{}) UID:{} - {}.'.format(index, user_id, str(data)))
            else:
                text.append('{}) {} - {}.'.format(index, get_username_or_name(user), str(data)))

        # And.. Now recall typing() It's soooo slooooow proooocess...
        typing(message)
        bot.edit_message_text('Статистика всех пользователей:\n' + '\n'.join(text),
                              stat_message.chat.id, stat_message.message_id)
    except:
        crash_message(message)
